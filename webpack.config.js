var path = require("path");
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: ["./src/js/app.js", "./src/scss/app.scss"],
  output: {
    path: path.resolve(__dirname, "dist/js"),
    filename: "app.js"
  },
  devServer: {
    inline: false,
    contentBase: "./dist"
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: "babel-loader",
        exclude: /(node_modules|bower_components)/,
        query: {
          presets: ["es2015"]
        }
      },
      {
        // https://gist.github.com/mburakerman/629783c16acf5e5f03de60528d3139af
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: "../css/app.css"
    })
  ]
};
